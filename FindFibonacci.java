package main;

import java.util.ArrayList;
import java.util.Scanner;

public class FindFibonacci {
	static final int MODULUS = 1000000007;
	
	static int addMod(int x, int y) {
		return (x + y) % MODULUS;
	}

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in); 
		
//		FindFibonacci();
		String stringIndex = scanner.nextLine();
		int index = Integer.parseInt(stringIndex);
		ArrayList<int[]> listInteger = new ArrayList<int[]>();
		for(int i =0 ;i < index;i++) {
			String s[]= scanner.nextLine().split(" ");
			int[] val = Line(s);
			listInteger.add(val);
		}
		
		System.out.println();
		System.out.println("Result:");
		
		for(int[] tempInt : listInteger) {
			System.out.println(FindingFibonacci(tempInt));
		}
		
		
	}
	public static int[] Line(String[] s) {
		int a[];
		a = new int[s.length];
		
		for(int i =0 ;i < s.length;i++){
		    a[i]= Integer.parseInt(s[i]);
		}
		return a;
	}
	
	public static int FindingFibonacci(int[] i) {
		int a = i[0];
		int b = i[1];
		int index = i[2];
		int fibo = 0;
		int result = 0;
//		Integer array[];
//		array = new Integer[index];
		for (int x = 0; x<= index; x++) {
//			if(x == Integer.valueOf(0)) {
//				array[x] = a;
//			}else if(x == Integer.valueOf(1)) {
//				array[x] = b;
//			}else {
//				Integer f1 = array[x-2];
//				Integer f2 = array[x-1];
//				array[x] = f1+f2; 
//			}
			if(x == 0) {
				result = a;
			}else if(x == 1) {
				result = b;
			}else {
				fibo =addMod(a, b);
				a = b;
				b = fibo;
				result = b;
						
			}

		}
		return result;

	}
	
}
	
